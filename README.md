Ansible Sygnal
=========

Ansible Role for https://github.com/matrix-org/sygnal.git

Requirements
------------

None

Role Variables
--------------

None (yet)

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: nordgedanken.sygnal

License
-------

GPLv3
